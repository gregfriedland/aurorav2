IDIR =../include
CC=g++
CFLAGS=-I$(IDIR) 
CPPFLAGS=-I$(IDIR) -O3 -Wall -I/usr/local/include/oscpack -I/usr/local/include

ODIR=src
LDIR =../lib

LIBS=-lm -lfftw3f -lasound -lgif -loscpack

_DEPS = util.h Drawer.h bzr.h ledWall.h SPI.h Array3D.h TestDrawer.h Palette.h rgb_hsv.h BeatDetect.h CircularArray.h FFT.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS)) 

_OBJ = util.o Drawer.o bzr.o ledWall.o SPI.o Palette.o rgb_hsv.o BeatDetect.o TextDrawer.o Perlin.o OSC.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


$(ODIR)/%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

ledWall: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(ODIR)/*~ $(IDIR)/*~ ledWall
