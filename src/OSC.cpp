#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "../include/OSC.h"
#include "../include/Drawer.h"
#include "../include/BeatDetect.h"
#include "../include/ledWall.h"
#include "../include/util.h"
#include <stdexcept>
//#include "ip/IpEndpointName.h"
#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"
#include "osc/OscOutboundPacketStream.h"
#include <sstream>
#include <iostream>

#define OUTPUT_BUFFER_SIZE 1024

void *socketRun(void *arg) {
  UdpListeningReceiveSocket *s = (UdpListeningReceiveSocket*) arg;
  printf("Spawned thread for OSC on port %d\n", OSC_IN_PORT);
  s->RunUntilSigInt();
  printf("Closing OSC thread\n");

  return NULL;
}

void LEDWallOSC::registerCommands() {
  cmdMap["/1"] = CMD_NONE;
  cmdMap["/2"] = CMD_NONE;

  cmdMap["/1/fader1"] = CMD_SET_SPEED;
  cmdMap["/1/fader2"] = CMD_SET_COLORCYCLING;
  cmdMap["/1/fader3"] = CMD_SET_CUSTOM1;
  cmdMap["/1/fader4"] = CMD_SET_CUSTOM2;
  cmdMap["/1/fader5"] = CMD_SET_PALETTE;
  cmdMap["/1/push1"] = CMD_NEXT_PROGRAM;
  cmdMap["/1/push2"] = CMD_NEXT_PALETTE;
  cmdMap["/1/push3"] = CMD_RESET;
  cmdMap["/1/rotary1"] = CMD_SET_BRIGHTNESS;
  cmdMap["/1/multixy1/1"] = CMD_SET_TOUCH1;
  cmdMap["/1/multixy1/2"] = CMD_SET_TOUCH2;
  cmdMap["/1/multixy1/3"] = CMD_SET_TOUCH3;
  cmdMap["/1/multixy1/4"] = CMD_SET_TOUCH4;
  cmdMap["/1/multixy1/5"] = CMD_SET_TOUCH5;

#if USE_AUDIO
  cmdMap["/2/multifader1/1"] = CMD_SET_AUDIO_SPEED_CHG1;
  cmdMap["/2/multifader1/2"] = CMD_SET_AUDIO_SPEED_CHG2;
  cmdMap["/2/multifader1/3"] = CMD_SET_AUDIO_SPEED_CHG3;
  cmdMap["/2/multifader2/1"] = CMD_SET_AUDIO_COLOR_CHG1;
  cmdMap["/2/multifader2/2"] = CMD_SET_AUDIO_COLOR_CHG2;
  cmdMap["/2/multifader2/3"] = CMD_SET_AUDIO_COLOR_CHG3;
  cmdMap["/2/multifader3/1"] = CMD_SET_AUDIO_SENSITIVITY1;
  cmdMap["/2/multifader3/2"] = CMD_SET_AUDIO_SENSITIVITY2;
  cmdMap["/2/multifader3/3"] = CMD_SET_AUDIO_SENSITIVITY3;
  cmdMap["/2/rotary1"] = CMD_SET_AUDIO_BEAT_LENGTH;
#endif
}

// send settings: those controls with 'fader' or 'rotary' in the address
void LEDWallOSC::sendSettings() {
  std::map<std::string, int>::iterator iter;
  for (iter = cmdMap.begin(); iter != cmdMap.end(); ++iter) {
    string addr = iter->first;
    if (addr.find("fader") != std::string::npos ||
	addr.find("rotary") != std::string::npos) {
      int cmd = iter->second;
      float val = ledWall->getSettingByCmd(cmd);

      std::ostringstream os;
      os << val;
      this->send(addr.c_str(), os.str().c_str());
    }
  }
}

void OscListener::ProcessMessage( const osc::ReceivedMessage& m, 
				  const IpEndpointName& _remoteEndpoint ) {
  remoteEndpoint = _remoteEndpoint;
  remoteEndpoint.port = OSC_OUT_PORT;

  try{
    float cmdArgs[MAX_CMD_ARGS];
    int numArgs = 0;
    for (osc::ReceivedMessage::const_iterator arg=m.ArgumentsBegin(); 
	 arg!=m.ArgumentsEnd(); arg++) {
      cmdArgs[numArgs] = arg->AsFloat();
      numArgs++;
    }
    
    try {
      int cmd = cmdMap->at(m.AddressPattern());

      
      ledWall->processCmd(cmd, cmdArgs, numArgs);
    } catch (std::out_of_range e) {
      std::cout << "received unknown OSC message: " << m.AddressPattern() << " ";
      for (int i=0; i<numArgs; i++) std::cout << cmdArgs[i];
      std::cout << "\n";
    }

#if 0    
    if (strcmp( m.AddressPattern(), "/1/fader1" ) == 0) {
      float val;
      args >> val >> osc::EndMessage;
      settings->speed = val;
      std::cout << "Setting speed to " << val << std::endl;
    } else if (strcmp( m.AddressPattern(), "/1/fader2" ) == 0) {
      float val;
      args >> val >> osc::EndMessage;
      settings->colorCycling = val;
      std::cout << "Setting color cycling to " << val << std::endl;
    } else if (strcmp( m.AddressPattern(), "/1/fader3" ) == 0) {
      float val;
      args >> val >> osc::EndMessage;
      settings->custom1 = val;
      std::cout << "Setting custom1 to " << val << std::endl;
    } else if (strcmp( m.AddressPattern(), "/1/fader4" ) == 0) {
      float val;
      args >> val >> osc::EndMessage;
      settings->custom2 = val;
      std::cout << "Setting custom2 to " << val << std::endl;
    } else {
      std::cout << "received uknown OSC message: " << m.AddressPattern() << std::endl;
    }
#endif
  } catch( osc::Exception& e ){
    // any parsing errors such as unexpected argument types, or 
    // missing arguments get thrown as exceptions.
    std::cerr << "error while parsing message: "
	      << m.AddressPattern() << ": " << e.what() << "\n";
  }
}

void LEDWallOSC::start() {
    socket = new UdpListeningReceiveSocket(
            IpEndpointName( IpEndpointName::ANY_ADDRESS, OSC_IN_PORT ),
            listener );

    pthread_t pth;
    int result = pthread_create(&pth, NULL, socketRun, (void*)socket);
    if (result != 0) {
      printf("Error spawning OSC thread\n");
    }

#if 0
    pid_t pid = fork();
    if (pid == 0) {                // child
      std::cout << "Forked OSC process for port " << OSC_IN_PORT << "\n";
      socket->RunUntilSigInt();
      std::cout << "OSC thread done\n";
      exit(0);
    } else if (pid < 0) {            // failed to fork
      std::cerr << "Failed to fork to listen for OSC packets" << std::endl;
    } else {                                   // parent
    }
#endif
  }

void LEDWallOSC::send(const char *addr, const char *msg) {
  IpEndpointName *endpoint = listener->getRemoteEndpoint();
  if (endpoint->address == IpEndpointName::ANY_ADDRESS) {
    printf("Attempted to send a message before knowing the destination address.\n");
    return;
  }

  if (transmitSocket == NULL || lastSendTimer.elapsedMillis() > 1000) {
    try {
      if (transmitSocket != NULL) {
	transmitSocket->~UdpTransmitSocket();
	transmitSocket = NULL;
      }
      transmitSocket = new UdpTransmitSocket(*endpoint);
    } catch (std::runtime_error &e) {
      std::cerr << "Error creating UDP socket for tansmission: " << e.what() << "\n";
      return;
    }
  }

  char buffer[OUTPUT_BUFFER_SIZE];
  osc::OutboundPacketStream p( buffer, OUTPUT_BUFFER_SIZE );

  p << osc::BeginBundleImmediate
    << osc::BeginMessage(addr) << msg << osc::EndMessage << osc::EndBundle;
  DPRINTF("Sending OSC msg: %s %s\n", addr, msg);

  transmitSocket->Send( p.Data(), p.Size() );
}
