#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include "../include/SPI.h"
#include "../include/util.h"
#include <bcm2835.h>

#define NOP() asm volatile("mov r0, r0")

//#define BITBANG_SPI

#ifdef BITBANG_SPI
// Adapted from https://github.com/Hive13/RPi-ShiftBrite/blob/abf6fc4c6f04031e2cc9dab16ea7e3d19e76133a/shiftbrite.c#L30
#define RPI_SPI_MOSI RPI_GPIO_P1_19 // GPIO 10
//#define RPI_SPI_MISO RPI_GPIO_P1_21 // GPIO 9
#define RPI_SPI_CLK  RPI_GPIO_P1_23 // GPIO 11
//#define RPI_SPI_CE0  RPI_GPIO_P1_24 // GPIO 8
//#define RPI_SPI_CE1  RPI_GPIO_P1_26 // GPIO 7
#endif

SPI::SPI(const char* dev, uint32_t speed) {
#ifdef BITBANG_SPI
  if(!bcm2835_init()) {
    printf("Error initializing BCM2835 library\n");
  }

  // Set up pins as outputs
  bcm2835_gpio_fsel(RPI_SPI_MOSI, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(RPI_SPI_CLK,  BCM2835_GPIO_FSEL_OUTP);

#else

#if 0
  if(!bcm2835_init()) {
    printf("Error initializing BCM2835 library\n");
  }
  bcm2835_spi_begin();
  bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
  bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_512);
  bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
#endif

  fd = open(dev, O_RDWR);
  if (fd < 0) {
    perror("Unable to connect to SPI device");
    exit(1);
  }

  int ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
  if (ret == -1) {
    perror("Unable to set SPI speed");
    exit(1);
  }

  ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
  if (ret == -1) {
    perror("Unable to set SPI speed");
    exit(1);
  }

#endif

  this->speed = speed;
}

void SPI::transfer(uint8_t *data, uint16_t nbytes) {
#ifdef BITBANG_SPI
  int polarity = 0;

  // Send most-significant bit first
  for (int j=0; j<nbytes; j++) {
    uint8_t val = data[j];
    for (int i=7; i>=0; i--) {
      bcm2835_gpio_write(RPI_SPI_CLK, polarity);
      //for (int k=0; k<250; k++) NOP();
      bcm2835_gpio_write(RPI_SPI_MOSI, (val >> i) & 0x01);
      for (int k=0; k<100; k++) NOP();;
      bcm2835_gpio_write(RPI_SPI_CLK, !polarity);
      //for (int k=0; k<250; k++) NOP();
      //bcm2835_gpio_write(RPI_SPI_MOSI, 0);
    }
  }
  bcm2835_gpio_write(RPI_SPI_CLK, polarity);

#else

#if 0
  // transfer using bcm2835 lib
  bcm2835_spi_transfern((char*)data, nbytes); // FIX: use diff receive buffer?
#endif

  if (write(fd, data, nbytes) != nbytes) {
    perror("SPI write error");
  }
#endif

  usleep(1000);
}


#if 0
void SPI::transfer(uint8_t *data, uint16_t nbytes) {
  int ret;
  uint8_t rx[nbytes];
  struct spi_ioc_transfer tr;
  tr.tx_buf = (unsigned long) data;
  tr.rx_buf = (unsigned long) rx;
  tr.len = nbytes;
  tr.delay_usecs = 500;
  tr.speed_hz = this->speed;
  tr.bits_per_word = 8;

  ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
  if (ret < 1) {
    perror("can't send spi message");
  }  
}
#endif
