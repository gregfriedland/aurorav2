#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "../include/bzr.h"
#include "../include/util.h"

#define PERTURB_BOX_SIZE 4
#define PERTURB_FREQ 10
#define TOUCH_RADIUS 2

Bzr::Bzr(uint16_t width, uint16_t height, DrawerSettings *settings) : 
  Drawer(width, height, settings) {
  aWidth = width * 4;
  aHeight = height * 4;

  a = new Array3D<float>(aWidth, aHeight, 2);
  b = new Array3D<float>(aWidth, aHeight, 2);
  c = new Array3D<float>(aWidth, aHeight, 2);

  //perturbCounter = 0;
} 

void Bzr::randomDots(uint16_t minx, uint16_t miny, 
		     uint16_t maxx, uint16_t maxy) {
  for (uint16_t x=minx; x<maxx; x++) {
    for (uint16_t y=miny; y<maxy; y++) {
      (*a)(x,y,p) = (random() % 1000) / 1000.0;
      (*b)(x,y,p) = (random() % 1000) / 1000.0;
      (*c)(x,y,p) = (random() % 1000) / 1000.0;
    }
  }
}

void Bzr::setAll(uint16_t minx, uint16_t miny, 
		 uint16_t maxx, uint16_t maxy,
		 float aval, float bval, float cval) {
  for (uint16_t x=minx; x<maxx; x++) {
    for (uint16_t y=miny; y<maxy; y++) {
      (*a)(x,y,p) = aval;
      (*b)(x,y,p) = bval;
      (*c)(x,y,p) = cval;
    }
  }
}

void Bzr::reset() { randomDots(0,0,aWidth,aHeight); }

void Bzr::draw() {
  Drawer::draw();

  uint16_t numStates = 20 - settings->getSpeed() * 19;
  if (state > numStates-1) state = numStates-1;

  //printf("%d\n", state);
  if (state == 0) {
    // every so often, stimulate the grid
    // off the main canvas to disturb repeating patterns
    if (random() % 100 < PERTURB_FREQ) {
      float aval = (random() % 1000) / 1000.0;
      float bval = (random() % 1000) / 1000.0;
      float cval = (random() % 1000) / 1000.0;
      setAll(aWidth*1/4-PERTURB_BOX_SIZE/2, aHeight*3/4-PERTURB_BOX_SIZE/2, 
	     aWidth*1/4+PERTURB_BOX_SIZE/2, aHeight*3/4+PERTURB_BOX_SIZE/2,
	     aval, bval, cval);
      setAll(aWidth*3/4-PERTURB_BOX_SIZE/2, aHeight*3/4-PERTURB_BOX_SIZE/2, 
	     aWidth*3/4+PERTURB_BOX_SIZE/2, aHeight*3/4+PERTURB_BOX_SIZE/2,
	     aval, bval, cval);
      setAll(aWidth*3/4-PERTURB_BOX_SIZE/2, aHeight*1/4-PERTURB_BOX_SIZE/2, 
	     aWidth*3/4+PERTURB_BOX_SIZE/2, aHeight*1/4+PERTURB_BOX_SIZE/2,
	     aval, bval, cval);
    }

    // stimulate the grid if we're touching 
    for (int i=0; i<MAX_TOUCHES; i++) {
      int x,y;
      if (settings->isTouching(i, &x, &y)) {
	setAll((x - TOUCH_RADIUS + aWidth) % aWidth, (y - TOUCH_RADIUS + aHeight) % aHeight,
	       (x + TOUCH_RADIUS + aWidth) % aWidth, (y + TOUCH_RADIUS + aHeight) % aHeight,
	       1, 1, 1);
      }
    }

    for (int x=0; x<aWidth; x++) {
      for (int y=0; y<aHeight; y++) {
	float c_a = 0;
	float c_b = 0;
	float c_c = 0;

	for (int i=x-1; i<=x+1; i++) {
	  int ii = (i + aWidth) % aWidth;
	  for (int j=y-1; j<=y+1; j++) {
	    int jj = (j + aHeight) % aHeight;
	    c_a += (*a)(ii,jj,p);
	    c_b += (*b)(ii,jj,p);
	    c_c += (*c)(ii,jj,p);
	  }
	}

	c_a /= 8.9;
	c_b /= 8.9;
	c_c /= 8.9;

	(*a)(x,y,q) = constrain( c_a + c_a * ( c_b - c_c ), 0.0, 1.0);
	(*b)(x,y,q) = constrain( c_b + c_b * ( c_c - c_a ), 0.0, 1.0);
	(*c)(x,y,q) = constrain( c_c + c_c * ( c_a - c_b ), 0.0, 1.0);
      }
    }

    if (p == 0) {
      p = 1;
      q = 0;
    } else {
      p = 0;
      q = 1;
    }
  }

  // the zoom should range from (0.05 to 1) * factor
  float zoom = 1; // (settings->custom2 * 0.95 + 0.05) * aWidth / width;
  //int lastX2 = -1, lastY2 = -1;
  for (uint16_t x=0; x<width; x++) {
    for (uint16_t y=0; y<height; y++) {
      int x2 = int(x * zoom);
      int y2 = int(y * zoom);

      // so we don't overwrite the same pixel twice
      //if (x2 == lastX2 && y == lastY2) continue;
      //lastX2 = x2;
      //lastY2 = y2;

      float a2;
      if (numStates == 1) {
	a2 = (*a)(x2,y2,q);
      } else {
	a2 = interp((*a)(x2,y2,q), (*a)(x2,y2,p), state, numStates);
	//if (x == (width-1) && y == (height-1)) {
	//  printf("state=%d, x2=%d, y2=%d, aq=%.3f, ap=%.3f, a2=%.3f\n", state, 
	//	 x2, y2, (*a)(x2,y2,q),
	//	 (*a)(x2,y2,p), a2);
	//}
      }

      int index = int(a2*(getPaletteSize()-1) * settings->custom1);
      setColor(x, y, index);

      //if (x == (width-1) && y == (height-1)) {
      //printf("state=%d, x2=%d, y2=%d, a2=%.3f, index=%d\n", state, 
      //       x2, y2, a2, index);
      //}

    }
  }

  state++;
  if (state > numStates-1) state = 0;
}

void Bzr::setup() {
  p = 0;
  q = 1;
  state = 0;

  randomDots(0, 0, aWidth, aHeight);
}
