#include "../include/Drawer.h"
#include "../include/util.h"
#include "../include/Array3D.h"
#include "../include/BeatDetect.h"
#include <math.h>
#include "../include/rgb_hsv.h"
#include "../include/ledWall.h"

Drawer::Drawer(uint16_t w, uint16_t h, DrawerSettings *_settings) {
  width = w;
  height = h;
  data = new Array3D<uint8_t>(3, w, h);
  settings = _settings;

  gamma = 0;
  setGamma(2.8);
  frameNum = 0;
}

void Drawer::draw() {
  frameNum++;
}

void Drawer::setColor(uint16_t x, uint16_t y, uint16_t index) {
  int palSize = settings->palette->getSize();

  // offset for color cycling
  int cyclingOffset = int(settings->colorCycling * MAX_COLOR_CYCLING_OFFSET) * frameNum;
  //index = (index + cyclingOffset) % palSize;

  // offset for audio beats
#if USE_AUDIO
  int audioOffset = 0;
  for (int i=0; i<settings->bd->getNumBands(); i++) {
    float indRange = float(palSize) / settings->bd->getNumBands(); 
    if (index >= i*indRange && index < (i+1)*indRange) {
      float smooth = sin(PI * (index - i*indRange) / indRange);
      audioOffset = smooth * settings->bd->getBeatPos(i) * MAX_AUDIO_COLOR_OFFSET * settings->audioColorChange[i];
      break;
    }
  }
  index = (index + cyclingOffset + audioOffset) % palSize;
#else
  index = (index + cyclingOffset) % palSize;
#endif

  uint8_t r, g, b;
  settings->palette->getRGB(index, &r, &g, &b);

  //if (settings->minSaturation > 0) {
  //uint8_t h, s, v;
  //rgbToHsv(r,g,b,&h,&s,&v);
  //s = constrain(s, settings->minSaturation, NUM_COLORS-1);
  //hsvToRgb(&r,&g,&b,h,s,v);
  //}

  //if (x == (width-1) && y == (height-1)) {
  //printf("%d %d %d %d\n", index, r, g, b);
  //}
  

  setColor(x, y, constrain(r, 0, settings->brightness),
       constrain(g, 0, settings->brightness), constrain(b, 0, settings->brightness));
}

void Drawer::setColor(uint16_t x, uint16_t y, uint8_t r, uint8_t g, uint8_t b) {
  // take into account how the wall is wired
  y = height - 1 - y;
  if (y%2 == 0) {
    x = width - 1 - x;
  }

#if 0
  // necessary only for Aurora v2
  // set pixel 139 pixel to zero and send it's data to the next pixel
  int n = y*width + x;
  if (n >= 373) {
    if (n == 373) {
      (*data)(0,x,y) = gammaTable[0];
      (*data)(1,x,y) = gammaTable[0];
      (*data)(2,x,y) = gammaTable[0];
    }

    n++;
    y = n/width;
    x = n%width;
  }
#endif

  (*data)(0,x,y) = gammaTable[r];
  (*data)(1,x,y) = gammaTable[g];
  (*data)(2,x,y) = gammaTable[b];
}

void Drawer::setGamma(float newGamma) {
  if (newGamma == gamma) return;
  
  gamma = newGamma;
  for (int i=0; i<NUM_COLORS; i++) {
    float d = ((float)i) / (NUM_COLORS-1);
    gammaTable[i] = floor((NUM_COLORS-1) * pow(d,gamma) + 0.5);
  }
}
