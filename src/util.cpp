#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "../include/util.h"
#include <sstream>
#include <string.h>
#include <math.h>


float dist(float x1, float y1, float x2, float y2) {
  float dx = x1-x2;
  float dy = y1-y2;
  return sqrt(dx*dx + dy*dy);
}

inline int getIndex(int ind, int length) { return (ind + length) % length; }

float calcMean(float *data, int dataLength, int startInd, int endInd) {
  float sum = 0;
  int diff = endInd - startInd + 1;
  if (endInd < startInd ) diff = endInd+dataLength - startInd + 1;

  for (int i=startInd; i<startInd+diff; i++) sum += data[getIndex(i, dataLength)];
  return sum / diff;
}  

float calcSD(float *data, int dataLength, int startInd, int endInd, float mean) {
  float sum = 0;
  int diff = endInd - startInd + 1;
  if (endInd < startInd ) diff = endInd+dataLength - startInd + 1;
  for (int i=startInd; i<startInd+diff; i++) 
    sum += pow(data[getIndex(i, dataLength)] - mean, 2);
  return sqrt(sum / diff);
}

float calcSD(float *data, int dataLength, int startInd, int endInd) {
  float mean = calcMean(data, dataLength, startInd, endInd);
  return calcSD(data, dataLength, startInd, endInd, mean);
}

double interp(double start, double end, uint32_t index, uint32_t length) {
  return end*index/length + start*(length-index)/length;
}

void splitRGB(uint32_t col, uint8_t *r, uint8_t *g, uint8_t *b) {
  *r = (col >> 16) & 0xFF;
  *g = (col >> 8) & 0xFF;
  *b = col & 0xFF;
}

uint32_t combRGB(uint8_t r, uint8_t g, uint8_t b) {
  uint32_t col = b;
  col += ((uint32_t)g) << 8;
  col += ((uint32_t)b) << 16;
  return col;
}

double constrain(double x, double low, double hi) {
  if (x <= low) 
    return low;
  else if  (x >= hi)
    return hi;
  return x;
}

int constrain(int x, int low, int hi) {
  if (x <= low) 
    return low;
  else if  (x >= hi)
    return hi;
  return x;
}

double*** mallocDouble3d(uint16_t x, uint16_t y, uint16_t z) {
  double ***array;
  array = (double***) malloc(x * sizeof(double *));
  if(array == NULL) {
    fprintf(stderr, "out of memory\n");
    exit(1);
  }

  for(uint16_t i=0; i<x; i++) {
    array[i] = (double**) malloc(y * sizeof(double *));
    if (array[i] == NULL) {
      fprintf(stderr, "out of memory\n");
      exit(1);
    }

    for (uint16_t j=0; j<y; j++) {
      array[i][j] = (double*) malloc(z * sizeof(double *));
      if (array[i][j] == NULL) {
	fprintf(stderr, "out of memory\n");
	exit(1);
      }
      memset(array[i][j], 0, z);
    }
  }
  return array;
}

uint8_t*** mallocByte3d(uint16_t x, uint16_t y, uint16_t z) {
  uint8_t ***array;
  array = (uint8_t***) malloc(x * sizeof(uint8_t *));
  if(array == NULL) {
    fprintf(stderr, "out of memory\n");
    exit(1);
  }

  for(uint16_t i=0; i<x; i++) {
    array[i] = (uint8_t**) malloc(y * sizeof(uint8_t *));
    if (array[i] == NULL) {
      fprintf(stderr, "out of memory\n");
      exit(1);
    }

    for (uint16_t j=0; j<y; j++) {
      array[i][j] = (uint8_t*) malloc(z * sizeof(uint8_t *));
      if (array[i][j] == NULL) {
	fprintf(stderr, "out of memory\n");
	exit(1);
      }
    }
  }
  return array;
}

clock_t millis() {
  return clock() / (CLOCKS_PER_SEC / 1000);
}

clock_t micros() {
  return clock() / (CLOCKS_PER_SEC / 1000000.0);
}

void rgbFromHex(string hex, uint8_t *r, uint8_t *g, uint8_t *b) {
  string rs = hex.substr(0,2);
  string gs = hex.substr(2,2);
  string bs = hex.substr(4,2);

  long rl = strtol(rs.c_str(), NULL, 16);
  long gl = strtol(gs.c_str(), NULL, 16);
  long bl = strtol(bs.c_str(), NULL, 16);

  *r = rl;
  *g = gl;
  *b = bl;
}  


