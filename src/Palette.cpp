#include "../include/Palette.h"
#include "../include/util.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <iterator>
#include <math.h>


void Palette::loadFromFile(const char *filename, uint16_t numColors, vector<PalettePtr> *palettes) {
  //string cs[] = { "FF0000", "00FF00", "0000FF", "FF0000" };
  //vector<string> cs2(cs, cs + 4);
  //PalettePtr p(new Palette(cs2, numColors));
  //palettes->push_back(p);

  string line;
  ifstream f(filename);
  if (f.is_open()) {
    while (f.good()) {
      getline(f,line);
      if (line.size() <= 0) break;

      // parse line into a vector of strings
#if 0
      istringstream iss(line);
      vector<string> tokens;
      copy(istream_iterator<string>(iss),
	   istream_iterator<string>(),
	   back_inserter< vector<string> >(tokens));
      tokens.push_back(tokens[0]);
#endif

      string buf;
      stringstream ss(line);
      vector<string> tokens;
      while (ss >> buf) tokens.push_back(buf);
      tokens.push_back(tokens[0]);

      PalettePtr p(new Palette(tokens, numColors));
      palettes->push_back(p);
      //std::cout << tokens[0] << std::endl;
    }
    f.close();
  } else {
    printf("Unable to palette file."); 
    exit(1);
  }

  std::cout << "Created " << palettes->size() << " palettes" << std::endl;
}

Palette::Palette(const vector<string> colorHexes, uint16_t _numColors) : numColors(_numColors) {
  colors = new Array2D<uint8_t>(_numColors, 3);
  int colorsPerHex = ceil(float(numColors) / (colorHexes.size() - 1));
  
  uint16_t ind = 0;
  uint8_t r1, g1, b1;
  //printf("\n");
  for (uint16_t h=0; h<colorHexes.size(); h++) {
    uint8_t r2,g2,b2;
    rgbFromHex(colorHexes[h], &r2, &g2, &b2);
    //printf("%d:  %d %d %d\n", h, r2, g2, b2);
    //splitRGB(col2, &r2, &g2, &b2);

    if (h != 0) {
      //std::cout << h << std::endl;
      int colorsThisHex = min(colorsPerHex, numColors - (h-1)*colorsPerHex);
      for (int i=0;i<colorsThisHex; i++) {
	uint8_t r, g, b;
	r = interp(r1, r2, i, colorsThisHex);
	g = interp(g1, g2, i, colorsThisHex);
	b = interp(b1, b2, i, colorsThisHex);
	//uint32_t col = combRGB(r,g,b);
	(*colors)(ind, 0) = r;
	(*colors)(ind, 1) = g;
	(*colors)(ind, 2) = b;

	//std::cout << "    " << ind << " " << int(r) << " " << int(g) << " " << int(b) << std::endl;
	ind++;
      }
    }

    r1 = r2;
    g1 = g2;
    b1 = b2;    
  }
}

//string Palette::toString() {
  
