// todo:
//   -audio sensitivity, color/speed change for test program

#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>

#include "../include/ledWall.h"
#include "../include/bzr.h"
#include "../include/TextDrawer.h"
#include "../include/PaintDrawer.h"

#if USE_AUDIO
  #include "../include/AudioTestDrawer.h"
#endif

#include "../include/OffDrawer.h"
#include "../include/GifTestDrawer.h"
#include "../include/AlienBlob.h"

#include "../include/util.h"
#include "../include/BeatDetect.h"
#include "../include/AudioInput.h"
#include <fcntl.h>
#include <string.h>
#include <sys/time.h>

// SPI
// green - CLOCK (Beaglebone P9: pin 31)
// white - MOSI  (Beaglebone P9: pin 30)
// black - GND   (Beaglebone P9: pins 43-46)
// red   - 5V    (Beaglebone P9: pins 5-6)

// Serial
// orange (red park cable RX) - BB pin 24 (UART1_TXD)
// green (red park cable TX) - BB pin 26 (UART1_RXD)

//#define SAMPLE_SIZE 512
#define SAMPLE_RATE 44100
#define DEFAULT_BZR_CUSTOM1

#define PRINT_FPS 0

enum Visuals {
  VIS_OFF,
  VIS_BZR,
  VIS_PAINT,
  VIS_TXT,
#if USE_AUDIO
  VIS_AUDIO_TEST,
#endif
  //VIS_GIF_TEST,
  VIS_ALIENBLOB,
  NUM_VISUALS,
};

LEDWall::LEDWall(uint16_t width, uint16_t height, float fps, 
		 const char* spiDev, const char *serialDev, uint32_t spiSpeed,
		 const char *paletteFile, uint16_t paletteSize, 
		 int startPalette, uint8_t startVisual, int audioFrames) {
  this->width = width;
  this->height = height;
  this->fps = fps;
  currVisual = startVisual;

#if USE_SERIAL  
  s = new Serial(serialDev);
  serialDataPtr = serialData;
  serialCmdStart = serialData;
#endif

  spi = new SPI(spiDev, spiSpeed);

#if USE_AUDIO
  bd = new BeatDetect(3, audioFrames, SAMPLE_RATE);
  audioInput = new AudioInput();
  if (audioInput->start(audioFrames) == -1)
    audioInput = NULL;
#endif

  Palette::loadFromFile(paletteFile, paletteSize, &palettes);
  if (startPalette < 0) {
    // pick random palette
    currPalette = random() % palettes.size();
  } else {
    currPalette = startPalette;
  }
  printf("Using palette %d\n", currPalette);

  DrawerSettings *settings;
  for (int i=0; i<NUM_VISUALS; i++) visuals.push_back(NULL);

  for (int i=0; i<NUM_VISUALS; i++) {
    settings = new DrawerSettings(1, 1, palettes[currPalette], currPalette, 
				  1, 1, 255, 0, fps);

#if USE_AUDIO
    settings->setBeatDetect(bd);
#endif

    switch(i) {
    case VIS_OFF:
      visuals[i] = new OffDrawer(width, height, settings);
      break;
      
      //    case VIS_TEST:
      //      visuals[i] = new TestDrawer(width, height, settings);
      //      break;

    case VIS_BZR:
      settings->speed = 0.7;
      settings->custom1 = 0.6;
      settings->custom2 = 0.25;
      settings->colorCycling = 0.1;
      //settings->audioSpeedChange[0] = 1;
      //settings->audioSpeedChange[1] = 0;
      //settings->audioSpeedChange[2] = 0;
      //settings->audioColorChange[0] = 0;
      //settings->audioColorChange[1] = 1;
      //settings->audioColorChange[2] = 0;
      //settings->setBeatLength(0, 0.4); // ~300
      //settings->setAudioSensitivity(0, 0.3); // 3sds
      visuals[i] = new Bzr(width, height, settings);
      break;

    case VIS_ALIENBLOB:
      settings->speed = 0.4;
      settings->custom1 = 0.6;
      settings->custom2 = 0.3;
      settings->colorCycling = 0.1;
      //settings->audioSpeedChange[0] = 1;
      //settings->audioSpeedChange[1] = 0;
      //settings->audioSpeedChange[2] = 0;
      //settings->audioColorChange[0] = 0;
      //settings->audioColorChange[1] = 1;
      //settings->audioColorChange[2] = 0;
      //settings->setBeatLength(0, 0.4); // ~300
      //settings->setAudioSensitivity(0, 0.3); // 3sds
      visuals[i] = new AlienBlob(width, height, settings);
      break;

    case VIS_PAINT:
      settings->colorCycling = 0.2;
      visuals[i] = new PaintDrawer(width, height, settings);
      break;

    case VIS_TXT:
      settings->text = "Welcome home! We missed you mommy. :)";
      settings->speed = 0.8;
      visuals[i] = new TextDrawer(width, height, settings);
      break;

#if USE_AUDIO
    case VIS_AUDIO_TEST:
      settings->speed = 0.1;
      visuals[i] = new AudioTestDrawer(width, height, settings);
      break;
#endif

#if 0
    case VIS_GIF_TEST:
      visuals[i] = new GifTestDrawer(width, height, settings);
      break;
#endif
    }
  }

  DrawerPtr drawer = visuals[currVisual];
  drawer->setup();

#if USE_OSC
  // init after visuals
  osc = new LEDWallOSC(this);
  osc->start();
  //osc->sendSettings();
#endif
}

void LEDWall::run() {
  Timer printTime;
  Timer loopTime;
  Timer recvTime;

  float interval = 1000000.0 / float(fps);
  int iter = 0;

  while(1) {
    if (loopTime.elapsedMicros() >= interval) {
      loopTime.reset();

#if USE_AUDIO
      // check if we've received a new set of audio input
      if (audioInput != NULL && audioInput->getUpdated()) {
	audioInput->setUpdated(false); // FIX: not thread safe
	bd->update(audioInput->getFloatData());
      }
#endif

      DrawerPtr drawer = visuals[currVisual];
      drawer->draw();
      spi->transfer(drawer->getData(), drawer->getDataLength());
      
      iter++;
#if PRINT_FPS      
      if (iter == PRINT_FPS) {
	printf("%.1f fps\n", ((float)iter) / printTime.elapsedMicros() * 1000000.0);
	printTime.reset();
	iter = 0;
      }
#endif
    } else {
      usleep(interval / 10);
    }

#if USE_SERIAL
    if (recvTime.elapsedMillis() >= 10) {
      receiveSerialCmds();
    }
#endif
  }
}        

float LEDWall::getSettingByCmd(int cmd) {
  DrawerSettings *s = visuals[currVisual]->getSettings();

  switch(cmd) {
  case CMD_SET_SPEED: return s->speed; break;
  case CMD_SET_COLORCYCLING: return s->colorCycling; break;
  case CMD_SET_CUSTOM1: return s->custom1; break;
  case CMD_SET_CUSTOM2: return s->custom2; break;
  case CMD_SET_PALETTE: return s->paletteNum / float(palettes.size()); break;
  case CMD_SET_BRIGHTNESS: return s->brightness / 255.0; break;

#if USE_AUDIO
  case CMD_SET_AUDIO_SPEED_CHG1: return s->audioSpeedChange[0]; break;
  case CMD_SET_AUDIO_SPEED_CHG2: return s->audioSpeedChange[1]; break;
  case CMD_SET_AUDIO_SPEED_CHG3: return s->audioSpeedChange[2]; break;
  case CMD_SET_AUDIO_COLOR_CHG1: return s->audioColorChange[0]; break;
  case CMD_SET_AUDIO_COLOR_CHG2: return s->audioColorChange[1]; break;
  case CMD_SET_AUDIO_COLOR_CHG3: return s->audioColorChange[2]; break;
  case CMD_SET_AUDIO_SENSITIVITY1: return s->getAudioSensitivity(0); break;
  case CMD_SET_AUDIO_SENSITIVITY2: return s->getAudioSensitivity(1); break;
  case CMD_SET_AUDIO_SENSITIVITY3: return s->getAudioSensitivity(2); break;
  case CMD_SET_AUDIO_BEAT_LENGTH: return s->getBeatLength(0); break;
#endif
  }

  std::cerr << "Attempted to incorrectly get setting by command " << cmd << "\n";
  return 0;
}

#if 0
#if USE_OSC
void LEDWall::updateOSCSettings() {
  DrawerSettings *s = visuals[currVisual]->getSettings();
  std::ostringstream os;

  os << s->speed;
  osc->send("/1/fader1", os.str().c_str());
  os << s->colorCycling;
  osc->send("/1/fader2", os.str().c_str());
  os << s->custom1;
  osc->send("/1/fader3", os.str().c_str());
  os << s->custom2;
  osc->send("/1/fader4", os.str().c_str());
  os << s->paletteNum;
  osc->send("/1/fader5", os.str().c_str());
  os << s->brightness;
  osc->send("/1/rotary1", os.str().c_str());

  os << s->audioSpeedChange[0];
  osc->send("/2/multifader1/1", os.str().c_str());
  os << s->audioSpeedChange[1];
  osc->send("/2/multifader1/2", os.str().c_str());
  os << s->audioSpeedChange[2];
  osc->send("/2/multifader1/3", os.str().c_str());
  os << s->audioColorChange[0];
  osc->send("/2/multifader2/1", os.str().c_str());
  os << s->audioColorChange[1];
  osc->send("/2/multifader2/2", os.str().c_str());
  os << s->audioColorChange[2];
  osc->send("/2/multifader2/3", os.str().c_str());
  os << bd->getAudioSensitivity(0);
  osc->send("/2/multifader3/1", os.str().c_str());
  os << bd->getAudioSensitivity(1);
  osc->send("/2/multifader3/2", os.str().c_str());
  os << bd->getAudioSensitivity(2);
  osc->send("/2/multifader3/3", os.str().c_str());
  os << bd->getBeatLength(0);
  osc->send("/2/rotary1", os.str().c_str());
}
#endif
#endif

#if USE_SERIAL
void LEDWall::receiveSerialCmds() {
  // read the data from the buffer
  uint16_t nbytes = s->receive(serialDataPtr, serialData+sizeof(serialData) - serialDataPtr - 1);

  // loop through it looking for command ends, which triggers the command actions
  for (int i=0; i<nbytes; i++) {
    printf("%d ", serialDataPtr[0]);
    if (serialDataPtr[0] == 255) {
      // we have a terminated command
      //processCmd(serialCmdStart, serialDataPtr - serialCmdStart);
      processSerialCmd(serialCmdStart[0], seralCmdStart+1, serialDataPtr - serialCmdStart - 1);
      serialCmdStart = serialDataPtr + 1;
    }
    serialDataPtr++;
  }

  // once we're done, copy the remaining bytes back to the start of the buffer
  int nbytesLeft = serialDataPtr - serialCmdStart;
  for (int i=0; i<nbytesLeft; i++) {
    serialData[i] = serialCmdStart[i];
  }
  serialCmdStart = serialData;
  serialDataPtr = serialData + nbytesLeft;
}

void LEDWall::processSerialCmd(uint8_t cmd, char *data, int nbytes) {
  float cmdArgs[MAX_CMD_ARGS];

  if (cmd == CMD_TEXT_ENTRY) {
    if (nbytes < 2) break;
    data[nbytes] = 0; // replace 255 with string terminator; this could be dangerous if we didn't know better
    printf("Text entry (%d bytes): %s\n", nbytes, data);
    drawer->getSettings()->text = data;
  } else if (cmd == CMD_SET_PROGRAM || cmd == CMD_SET_PALETTE) {
    cmdArgs[0] = data[i];
    processCmd(cmd, cmdArgs, 1);
  } else {
    if (nbytes >= MAX_CMD_ARGS) {
      printf("Serial Error: received too much data for cmd %d: %d bytes\n", cmd, nbytes);
      return;
    }

    for (int i=0; i<nbytes; i++) {
      cmdArgs[i] = data[i]/254.0;
    }
    
    processCmd(cmd, cmdArgs, nbytes);
  }
}
#endif

void LEDWall::processCmd(uint8_t cmd, float *cmdArgs, int numArgs) {
  DrawerPtr drawer = visuals[currVisual];
  float val, val2;
  int iVal;
  std::ostringstream os;
    
  switch(cmd) {
  case CMD_NONE:
    break;

  case CMD_NEXT_PROGRAM: 
    if (numArgs < 1 || cmdArgs[0] != 1) break;
    DPRINTF("Next program\n");
    currVisual = (currVisual + 1) % visuals.size();
    drawer = visuals[currVisual];
    drawer->setup();

#if USE_OSC
    osc->sendSettings();
    os << "Palette " << drawer->getSettings()->paletteNum;
    osc->send("/1/palette", os.str().c_str());
#endif
    break;

  case CMD_NEXT_PALETTE:
    if (numArgs < 1 || cmdArgs[0] != 1) break;
    DPRINTF("Next palette\n");
    currPalette = (currPalette + 1) % palettes.size();
    drawer->getSettings()->palette = palettes[currPalette];
    drawer->getSettings()->paletteNum = currPalette;
    break;

  case CMD_RESET:
    if (numArgs < 1 || cmdArgs[0] != 1) break;
    DPRINTF("Reset\n");
    drawer->reset();
    break;

  case CMD_SET_SPEED:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set speed: %.2f\n", val);
    drawer->getSettings()->speed = val;
    break;

  case CMD_SET_COLORCYCLING:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set color cycling: %.2f\n", val);
    drawer->getSettings()->colorCycling = val;
    break;

  case CMD_SET_CUSTOM1:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set custom1: %.2f\n", val);
    drawer->getSettings()->custom1 = val;
    break;

  case CMD_SET_CUSTOM2:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set custom2: %.2f\n", val);
    drawer->getSettings()->custom2 = val;
    break;

  case CMD_SET_BRIGHTNESS:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set brightness: %.2f\n", val);
    drawer->getSettings()->brightness = val * 255;
    break;

  case CMD_SET_MIN_SATURATION:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set min saturation: %.2f\n", val);
    drawer->getSettings()->minSaturation = val*255;
    break;

  case CMD_SET_PROGRAM:
    if (numArgs < 1) break;
    iVal = cmdArgs[0];
    if ((uint32_t)iVal >= visuals.size()) break;
    DPRINTF("Set program : %d\n", iVal);
    currVisual = iVal;
    drawer = visuals[currVisual];
    drawer->setup();

#if USE_OSC
    osc->sendSettings();
    os << "Palette " << drawer->getSettings()->paletteNum;
    osc->send("/1/palette", os.str().c_str());
#endif
    break;

  case CMD_SET_PALETTE:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    currPalette = round(val * (palettes.size()-1));
    if (currPalette >= palettes.size()) break;
    DPRINTF("Set palette: %.d\n", currPalette);
    drawer->getSettings()->palette = palettes[currPalette];
    drawer->getSettings()->paletteNum = currPalette;

#if USE_OSC
    os << "Palette " << currPalette;
    osc->send("/1/palette", os.str().c_str());
#endif
    break;

#if USE_AUDIO
  case CMD_SET_AUDIO_BEAT_LENGTH:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set beat length: %.2f\n", val);
    drawer->getSettings()->setBeatLength(0, val);
    drawer->getSettings()->setBeatLength(1, val);
    drawer->getSettings()->setBeatLength(2, val);
    break;

  case CMD_SET_AUDIO_SENSITIVITY1:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio sensitivity1: %.2f\n", val);
    drawer->getSettings()->setAudioSensitivity(0, val);
    break;

  case CMD_SET_AUDIO_SENSITIVITY2:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio sensitivity2: %.2f\n", val);
    drawer->getSettings()->setAudioSensitivity(1, val);
    break;

  case CMD_SET_AUDIO_SENSITIVITY3:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio sensitivity3: %.2f\n", val);
    drawer->getSettings()->setAudioSensitivity(2, val);
    break;

  case CMD_SET_AUDIO_COLOR_CHG1:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio color change1: %.2f\n", val);
    drawer->getSettings()->audioColorChange[0] = val;
    break;

  case CMD_SET_AUDIO_COLOR_CHG2:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio color change2: %.2f\n", val);
    drawer->getSettings()->audioColorChange[1] = val;
    break;

  case CMD_SET_AUDIO_COLOR_CHG3:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio color change3: %.2f\n", val);
    drawer->getSettings()->audioColorChange[2] = val;
    break;

  case CMD_SET_AUDIO_SPEED_CHG1:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio speed change1: %.2f\n", val);
    drawer->getSettings()->audioSpeedChange[0] = val;
    break;

  case CMD_SET_AUDIO_SPEED_CHG2:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio speed change2: %.2f\n", val);
    drawer->getSettings()->audioSpeedChange[1] = val;
    break;

  case CMD_SET_AUDIO_SPEED_CHG3:
    if (numArgs < 1) break;
    val = cmdArgs[0];
    DPRINTF("Set audio speed change3: %.2f\n", val);
    drawer->getSettings()->audioSpeedChange[2] = val;
    break;
#endif

  case CMD_SET_TOUCH1:
    if (numArgs < 2) break;
    val = cmdArgs[0];
    val2 = cmdArgs[1];
    DPRINTF("Touch 1 %.2f %.2f\n", val, val2);
    drawer->getSettings()->setTouch(0, round(val*width), round(val2*height));
    break;

  case CMD_SET_TOUCH2:
    if (numArgs < 2) break;
    val = cmdArgs[0];
    val2 = cmdArgs[1];
    DPRINTF("Touch 2 %.2f %.2f\n", val, val2);
    drawer->getSettings()->setTouch(1, round(val*width), round(val2*height));
    break;

  case CMD_SET_TOUCH3:
    if (numArgs < 2) break;
    val = cmdArgs[0];
    val2 = cmdArgs[1];
    drawer->getSettings()->setTouch(2, round(val*width), round(val2*height));
    DPRINTF("Touch 3 %.2f %.2f\n", val, val2);
    break;

  case CMD_SET_TOUCH4:
    if (numArgs < 2) break;
    val = cmdArgs[0];
    val2 = cmdArgs[1];
    DPRINTF("Touch 4 %.2f %.2f\n", val, val2);
    drawer->getSettings()->setTouch(3, round(val*width), round(val2*height));
    break;

  case CMD_SET_TOUCH5:
    if (numArgs < 2) break;
    val = cmdArgs[0];
    val2 = cmdArgs[1];
    DPRINTF("Touch 5 %.2f %.2f\n", val, val2);
    drawer->getSettings()->setTouch(4, round(val*width), round(val2*height));
    break;

  default:
    printf("Unknown command %d:", cmd);
    for (int i=0; i<numArgs; i++) {
      printf("%f ", cmdArgs[i]);
    }
    printf("\n");
    break;
  }
}


int main(int argc, char **argv) {
  int opt, p=-1, v=VIS_BZR, a=512;
  int fps = 45;

  struct timeval tv;
  gettimeofday(&tv, NULL);
  srandom(tv.tv_sec * tv.tv_usec);

  while ((opt = getopt (argc, argv, "f:p:v:a:")) != -1) {
    switch (opt) {
    case 'f':
      fps = atoi(optarg);
      break;
    case 'p':
      p = atoi(optarg);
      break;
    case 'v':
      v = atoi(optarg);
      break;
    case 'a':
      a = atoi(optarg);
      break;
    default:
      abort();
    }
  }

  assert(argc - optind > 0);
  const char *colorFile = argv[optind];

  printf("Started\n");

  const char *spiDevice = "/dev/spidev0.0";
  const char *serialDevice = "/dev/ttyO1";
  LEDWall ledWall(32, 18, fps, spiDevice, serialDevice, 1000000, 
		  colorFile, 1024, p, v, a);
  ledWall.run();
}
     
