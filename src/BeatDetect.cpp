// Class to detect beats
// Implementation by Greg Friedland in 2012 based on methods described by Simon Dixon in 
// "ONSET DETECTION REVISITED" Proc. of the 9th Int. Conference on Digital Audio Effects (DAFx-06), Montreal, Canada, September 18-20, 2006
// (http://www.dafx.ca/proceedings/papers/p_133.pdf)

#include "../include/BeatDetect.h"
#include <math.h>

static const int numNeighbors[] = { 1, 5, 5 }; 

BeatDetect::BeatDetect(int _numBands, int _sampleSize, int _sampleRate) 
  : numBands(_numBands), sampleSize(_sampleSize), sampleRate(_sampleRate) {
  fft = new FFT(sampleSize, sampleRate);
  float minFreq = max(MIN_FREQ, sampleSize/sampleRate);

  float logBandwidth = (log(MAX_FREQ) - log(minFreq)) / (numBands - 1);
  for (int i=0; i<numBands; i++) {
    threshSensitivity.push_back(DEFAULT_THRESH_SENSITIVITY);
    beatLength.push_back(DEFAULT_BEAT_LENGTH);
    analyzeBand.push_back(true);
    lastOnsetTimer.push_back(Timer());

    bandFreq.push_back(minFreq * exp(i*logBandwidth));
    printf("Band %d using frequency %.0f\n", i, bandFreq[i]);
  }
        
  // map fft bands (samplesize/2+1) to our band definition
  //for (int i=0; i<fft->getNumBands(); i++) {
  //  float freq = fft->indexToFreq(i);
  //  int ind = max(round(log(freq/minFreq) / logBandwidth), 0.0);
    //fftBandMap.push_back(ind);
  //  printf("Mapping fft band %d with freq %.0f to band %d\n", i, freq, ind);
  //}

  for (int i=0; i<numBands; i++) {
    spectralFlux.push_back(new CircularArrayWithAvgs(HISTORY_SIZE, SHORT_HISTORY_SIZE, 
						 LONG_HISTORY_SIZE));
    spectralFluxSD.push_back(new CircularArrayWithAvgs(HISTORY_SIZE, SHORT_HISTORY_SIZE, 
						   LONG_HISTORY_SIZE));
    onsetHist.push_back(new CircularArray(HISTORY_SIZE));
    threshold.push_back(new CircularArray(HISTORY_SIZE));
    spectrum.push_back(new CircularArray(HISTORY_SIZE));
  }
}

// samples should be length SAMPLE_SIZE; same as FFT N
void BeatDetect::update(float *samples) {
  fft->transform(samples);

  int ind = spectralFlux[0]->getIndex();
  for (int i=0; i<numBands; i++) {
    if (!analyzeBand[i]) continue;
      
    // multiply neighbors
    int fftBand = fft->freqToIndex(bandFreq[i]);
    float val = 1;
    for (int j=max(0, fftBand-numNeighbors[i]); 
	 j<=min(fft->getNumBands()-1, fftBand+numNeighbors[i]); j++) {
      val *= fft->getRealOutput(j);
    }
    spectrum[i]->add(val);

    // calculate spectral flux     
    float diff = abs(spectrum[i]->get()) - abs(spectrum[i]->getPrev());
    spectralFlux[i]->add((diff + abs(diff)) / 2.0);
    spectralFluxSD[i]->add(spectralFlux[i]->sd(ind-LONG_HISTORY_SIZE+1, ind));

    float thresh = spectralFlux[i]->getEMA2(ind-1) + threshSensitivity[i] * spectralFluxSD[i]->getEMA2(ind-1);
    threshold[i]->add(thresh);

    if (spectralFlux[i]->getEMA1(ind) > max(thresh, MIN_THRESHOLD) && 
	lastOnsetTimer[i].elapsedMillis() >= beatLength[i] ) {
      onsetHist[i]->add(true);
      //println("beat gap" + (i+1) + ": " + (millis() - lastOnsetTimes[i]));
      lastOnsetTimer[i].reset();
    } else {
      onsetHist[i]->add(false);
    }
  }    
}
 

