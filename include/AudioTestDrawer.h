#ifndef AUDIOTESTDRAWER_H
#define AUDIOTESTDRAWER_H

#include <stdint.h>
#include "util.h"
#include <vector>
#include "Drawer.h"

#define MAX_RADIUS 3

class AudioTestDrawer : public Drawer {
 public:
  AudioTestDrawer(uint16_t width, uint16_t height, DrawerSettings *settings) : 
  Drawer(width, height, settings) {
    numBalls = 3;

    for (int i=0; i<numBalls; i++) {
      radii.push_back(1);
      dradii.push_back(0.2);
    }
  }

  void setup() {
    for (int i=0; i<numBalls; i++) {
      x.push_back((i+1)*width/4.0);
      y.push_back(1.0/3*height);
    }
  }

  void reset() { }

  void draw() {
    setColorAll(0,0,0);

    for (int i=0; i<numBalls; i++) {
      radii[i] += dradii[i] * settings->getSpeed();
      if (radii[i] >= MAX_RADIUS || radii[i] <= 0) { 
	dradii[i] = -dradii[i]; 
      }

      y[i] = getYPos(settings->bd->getBeatPos(i), radii[i]);
      drawBall(radii[i], x[i], y[i], settings->palette->getSize() * (i + 0.5) / numBalls);
    }
  }

  // fractional radius on pixelated bounds?
  void drawBall(float radius, int centerX, int centerY, int index) {
    for (int x=MAX(0, centerX - radius); x<MIN(width, centerX + radius); x++) {
      for (int y=MAX(0, centerY - radius); y<MIN(width, centerY + radius); y++) {
	setColor(x,y,index);
      }
    }
  }

  float getYPos(float t, float radius) {
    float y0 = 1.0/3.0*height;
    float y1 = height - radius;
    float a = (y1-y0)*8;
    
    if (t > 0.5) t = 1 - t;
    return y0 + 1.0/2.0*a*t*t;
  }    

 private:
  vector<float> x, y;
  vector<float> radii;
  vector<float> dradii;
  int numBalls;
};

#endif

