#ifndef FFT_H
#define FFT_H

#include <fftw3.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>

class FFT {
 public:
 FFT(int _N, float _sampleRate) : N(_N), sampleRate(_sampleRate) {
    in = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * N);
    out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * N);
    p = fftwf_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    numBands = N / 2 + 1;
    count = 0;
    powerSpectrum = (float*) malloc(sizeof(float) * (N/2+1));
  }
  ~FFT() {
    free(powerSpectrum);
    fftwf_destroy_plan(p);
    fftwf_free(in); 
    fftwf_free(out);
  }

  // input should be length N; output should be length numBands
  void transform(float *input) {
    for (int i=0; i<N; i++) {
      in[i][0] = input[i];
      in[i][1] = 0;
    }
    fftwf_execute(p);

    int k;
    powerSpectrum[0] = out[0][0]*out[0][0];  /* DC component */
    for (k = 1; k < (N+1)/2; ++k)  /* (k < N/2 rounded up) */
      powerSpectrum[k] = out[k][0]*out[k][0] + out[N-k][0]*out[N-k][0];
    if (N % 2 == 0) /* N is even */
      powerSpectrum[N/2] = out[N/2][0]*out[N/2][0];  /* Nyquist freq. */
    
    //if (count % 25 == 0) {
    //  float freqs[] = { 100, 300, 1000, 3000, 10000};
    //  for (int i=0; i<5; i++) {
    //	printf("%8.1e ", powerSpectrum[freqToIndex(freqs[i])]);
    // }
    //  printf("\n");;
    //}
    count++;
  }

  float getBandWidth() {
    return float(sampleRate) / N;
  }

  float indexToFreq(int i) {
    assert(i >= 0 && i < numBands);
    float bw = getBandWidth();

    // the width of the first and last bins are half that of the others.
    if (i == 0) return bw / 4.0;
    if (i == numBands - 1) {
      float lastBinBeginFreq = (sampleRate / 2.0) - (bw / 2.0);
      float binHalfWidth = bw / 4.0;
      return lastBinBeginFreq + binHalfWidth;
    }

    return i*bw;
  }

  int freqToIndex(float freq) {
    float bw = getBandWidth();
    if (freq < bw / 2) return 0;
    if (freq > sampleRate / 2 - bw / 2) return numBands - 1;

    return round(freq / bw);
  }

  int getN() { return N; }
  int getNumBands() { return numBands; }

  inline float getRealOutput(int i) { return out[i][0]; }

 private:
  fftwf_complex *in, *out;
  fftwf_plan p;
  int N, numBands;
  float sampleRate;
  int count;
  float *powerSpectrum;
};

#endif
