#ifndef GIFTESTDRAWER_H
#define GIFTESTDRAWER_H

#include <stdint.h>
#include <string>
#include "Drawer.h"
#include "Gif.h"

class GifTestDrawer : public Drawer {
 public:
  GifTestDrawer(uint16_t width, uint16_t height, DrawerSettings *settings) : 
    Drawer(width, height, settings) {
      filename = "/home/pi/AuroraLEDWall/phage-logo-4.gif";
    gif = new Gif(filename.c_str());
  }

  void setup() {
    frame = 0;
  }

  void reset() { frame = 0; }

  void draw() {
    Drawer::draw();

    int framesPerUpdate = settings->getFPS() * (1 - settings->speed) + 1;
      if (frameNum % framesPerUpdate == 0) {
	gif->loadIntoArray(data, frame);
	frame = (frame + 1) % gif->getNumFrames();
      }
  }

 private:
  string filename;
  Gif *gif;
  int frame;
};

#endif
