#ifndef TESTDRAWER_H
#define TESTDRAWER_H

#include <stdint.h>
#include "Drawer.h"

class TestDrawer : public Drawer {
 public:
  TestDrawer(uint16_t width, uint16_t height, DrawerSettings *settings) : 
    Drawer(width, height, settings) {
  }

  void setup() {
    i = 0;
    di = 5;
    dj = 20;
  }

  void reset() { i = 0; }

  void draw() {
    Drawer::draw();

    //printf("%d\n", i);
    for (uint16_t y=0; y<height; y++) {
      int j = 0;
      for (uint16_t x=0; x<width; x++) {
	setColor(x,y,i+j);
	j += dj;
      }
    }
    i += di;
    if (i >= getPaletteSize()) {
      i = 0;
    }
  }

 private:
  int i, di,dj;
};

#endif
