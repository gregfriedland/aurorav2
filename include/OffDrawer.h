#ifndef OFFDRAWER_H
#define OFFDRAWER_H

#include <stdint.h>
#include "Drawer.h"
#include <unistd.h>

class OffDrawer : public Drawer {
 public:
  OffDrawer(uint16_t width, uint16_t height, DrawerSettings *settings) : 
    Drawer(width, height, settings) {
  }

  void setup() {}
  void reset() {}

  void draw() {
    for (uint16_t y=0; y<height; y++) {
      for (uint16_t x=0; x<width; x++) {
	setColor(x, y, 0, 0, 0);
      }
    }
    usleep(20000);
  }
};

#endif
