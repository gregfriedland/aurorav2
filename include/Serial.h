#ifndef SERIAL_H
#define SERIAL_H

#include <stdint.h>
#include "Drawer.h"
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#define MAX_SERIAL_DATA_SIZE 1024
#define BAUDRATE B9600

class Serial {
 public:
  Serial(const char *_device) {
    device = _device;
    fd = -1;
  }

  int init() {
    int fd2 = open(device, O_RDONLY | O_NOCTTY);// | O_NONBLOCK);
    if (fd2 == -1) {
      perror("open_port: Unable to open serial device: ");
      return fd2;
    }

    //    fcntl(fd2, F_SETFL, 0);

    struct termios options;

    /*
     * Get the current options for the port...
     */
    //tcgetattr(fd2, &options);

    /*
     * Set the baud rates to 9600...
     */
    //cfsetispeed(&options, B9600);
    //cfsetospeed(&options, B9600);

    /*
     * Enable the receiver and set local mode...
     */

    bzero(&options, sizeof(options));
    options.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    options.c_cc[VTIME]    = 0;
    options.c_cc[VMIN]     = 0;

    tcflush(fd2, TCIFLUSH);

    //options.c_cflag |= (CLOCAL | CREAD);

    /*
     * Set the new options for the port...
     */
    tcsetattr(fd2, TCSANOW, &options);

    return fd2;
  }

  // data should be MAX_SERIAL_DATA_SIZE
  // reads data until there is none left in the Serial buffer
  int receive(char *data, int maxBytes) {
    if (fd == -1) {
      fd = init();
      if (fd == -1) return 0;

      printf("Opened serial connection\n");
    }

    return read(fd, data, maxBytes);
  }

 private:
  const char *device;
  int fd;
  //char buffer[MAX_SERIAL_DATA_SIZE];
  //char *bufptr;
};  
#endif
