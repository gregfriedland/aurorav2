#ifndef RGB_HSV_H
#define RGB_HSV_H

void rgbToHsv(uint8_t r, uint8_t g, uint8_t b, uint8_t *h, uint8_t *s, uint8_t *v);
void hsvToRgb(uint8_t *r, uint8_t *g, uint8_t *b, uint8_t h, uint8_t s, uint8_t v);

#endif
