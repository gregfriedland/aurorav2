#ifndef AUDIO_H
#define AUDIO_H

/* Use the newer ALSA API */
#define ALSA_PCM_NEW_HW_PARAMS_API

#include <alsa/asoundlib.h>
#include <stdbool.h>
#include <stdint.h>

class AudioInput {
 public:
  AudioInput() {
  }

  int start(int nFrames) {
    int rc;
    snd_pcm_hw_params_t *params;    
    unsigned int val;
    int dir;

    frames = nFrames;

    /* Open PCM device for recording (capture). */
    rc = snd_pcm_open(&handle, "default",
		      SND_PCM_STREAM_CAPTURE, SND_PCM_NONBLOCK);
    if (rc < 0) {
      fprintf(stderr,
	      "unable to open pcm device: %s\n",
	      snd_strerror(rc));
      return -1;
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(handle, params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(handle, params,
				 SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(handle, params,
				 SND_PCM_FORMAT_U16_LE);

    /* Mono */
    snd_pcm_hw_params_set_channels(handle, params, 1);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 44100;
    snd_pcm_hw_params_set_rate_near(handle, params,
				    &val, &dir);

    /* Set period size in frames. */
    snd_pcm_hw_params_set_period_size_near(handle,
					   params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(handle, params);
    if (rc < 0) {
      fprintf(stderr,
	      "unable to set hw parameters: %s\n",
	      snd_strerror(rc));
      return -1;
    }

    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(params,
				      &frames, &dir);
    size = frames * 2; /* 2 bytes/sample, 1 channel */
    buffer = (char *) malloc(sizeof(char) * size);
    floatBuffer = (float *) malloc(sizeof(float) * frames);

    pthread_create(&thread,NULL,AudioInput::updateWrapper,this);

    updated = false;
    return 0;
  }

  static void *updateWrapper(void *audioInput) {
    ((AudioInput *)audioInput)->update();
    return NULL;
  }

  void update() {
    while(1) {
      int rc = snd_pcm_readi(handle, buffer, frames);
      if (rc == -EPIPE) {
	/* EPIPE means overrun */
	fprintf(stderr, "overrun occurred\n");
	snd_pcm_prepare(handle);
      } else if (rc < 0) {
	if (rc != -11) // i.e. Resource temporarily unavailable
	  fprintf(stderr,
		  "error from read %d: %s\n",
		  rc,
		  snd_strerror(rc));
      } else if (rc != (int)frames) {
	fprintf(stderr, "short read, read %d frames\n", rc);
      } else {
	updated = true;

	for (uint32_t i=0; i<frames; i++) {
	  // normalize the little endian char pairs to 0 - 65535 then to -1 to 1
	  floatBuffer[i] = (buffer[2*i] + buffer[2*i+1] * 256UL) / 32768.0 - 1;
	}

      }

      usleep(1000);
    }
  }

  // gets the float data from the last update
  float *getFloatData() { 
    return floatBuffer; 
  }

  bool getUpdated() { return updated; }
  void setUpdated(bool _updated) { updated = _updated; }

  void stop() {
    pthread_cancel(thread);

    snd_pcm_drain(handle);
    snd_pcm_close(handle);
    free(buffer);
    free(floatBuffer);
  }

 private:
  snd_pcm_t *handle;
  char *buffer;
  snd_pcm_uframes_t frames;
  int size;
  pthread_t thread;
  bool updated;
  float *floatBuffer;
};

#endif
