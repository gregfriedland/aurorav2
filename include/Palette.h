#ifndef PALETTE_H
#define PALETTE_H

using namespace std;

#include <stdint.h>
#include "Array2D.h"
#include <vector>
#include <string>
#include <assert.h>
//#include <boost/shared_ptr.hpp>

class Palette;

//typedef boost::shared_ptr<Palette> PalettePtr;
typedef Palette* PalettePtr;

class Palette {
 public:
  static void loadFromFile(const char *filename, uint16_t numColors, 
			   vector<PalettePtr> *palettes);
  Palette(const vector<string> colorHexes, uint16_t numColors);
  ~Palette() { delete colors; }
  void getRGB(uint16_t index, uint8_t *r, uint8_t *g, uint8_t *b) { 
    assert(index < numColors);
    *r = (*colors)(index, 0);
    *g = (*colors)(index, 1);
    *b = (*colors)(index, 2);
  }
  uint16_t getSize() { return numColors; }

 protected:
  Array2D<uint8_t> *colors;
  uint16_t numColors;
};

#endif
