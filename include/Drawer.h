#ifndef DRAWER_H
#define DRAWER_H

#include <stdint.h>
#include <string>
#include <vector>
#include "Array3D.h"
#include "Palette.h"
#include "BeatDetect.h"
#include "util.h"

#define IS_TOUCHING_TIMEOUT 100 // # of milliseconds which counts for a touch
#define MAX_TOUCHES 5
#define NUM_COLORS 256
//#define MIN_SATURATION 200
#define DEFAULT_GAMMA 3.0
#define MAX_COLOR_CYCLING_OFFSET 50

class DrawerSettings {
 public:
  DrawerSettings(float _speed, float _colorCycling, PalettePtr _palette, 
		 int _paletteNum, float _custom1, float _custom2, int _brightness, 
		 int _minSaturation, int _fps) {
    fps = _fps;
    speed =_speed;
    colorCycling = _colorCycling;
    palette = _palette;
    custom1 = _custom1;
    custom2 = _custom2;
    brightness = _brightness;
    minSaturation = _minSaturation;
    paletteNum = _paletteNum;
    
    for (int i=0; i<MAX_TOUCHES; i++) {
      touchX.push_back(0);
      touchY.push_back(0);
      lastTouchTimer.push_back(Timer());
    }

#if USE_AUDIO
    for (int i=0; i<bd->getNumBands(); i++) {
      audioSpeedChange.push_back(0);
      //audioSensitivity.push_back(0);
      audioColorChange.push_back(0);
      //beatLength.push_back(0);
    }
#endif
  }

#if USE_AUDIO
  void setBeatDetect(BeatDetect *_bd) {
    bd = _bd;
  }
#endif

  bool isTouching(int touchNum, int *x, int *y) {
    if (lastTouchTimer[touchNum].elapsedMillis() < IS_TOUCHING_TIMEOUT) {
      *x = touchX[touchNum];
      *y = touchY[touchNum];
      return TRUE;
    }
    return FALSE;
  }

  void setTouch(int touchNum, int x, int y) {
    touchX[touchNum] = x;
    touchY[touchNum] = y;
    lastTouchTimer[touchNum].reset();
  }

  float getSpeed() {
    float speed2 = speed;

#if USE_AUDIO
    for (int i=0; i<bd->getNumBands(); i++) {
      speed2 += (bd->getBeatPos(i) > 0) * audioSpeedChange[i];
    }
#endif
    return constrain(speed2, 0.0, 1.0);
  }

#if USE_AUDIO
  void setBeatLength(int band, float factor) { 
    bd->setBeatLength(band, MAX_BEAT_LENGTH * factor);
  }
  float getBeatLength(int band) { 
    return bd->getBeatLength(band) / float(MAX_BEAT_LENGTH);
  }

  void setAudioSensitivity(int band, float factor) { 
    bd->setAudioSensitivity(band, MAX_THRESH_SENSITIVITY * factor);
  }
  float getAudioSensitivity(int band) { 
    return bd->getAudioSensitivity(band) / float(MAX_THRESH_SENSITIVITY);
  }
#endif

  float getFPS() { return fps; }

  float colorCycling, custom1, custom2;
  int brightness, minSaturation, paletteNum;
  PalettePtr palette;
  string text;

#if USE_AUDIO
  vector<float> audioSpeedChange, /*audioSensitivity, */audioColorChange;//, beatLength;
  BeatDetect *bd;
#endif

  float speed;
 private:
  float fps;
  vector<float> touchX, touchY;
  vector<Timer> lastTouchTimer;
};

class Drawer {
 public:
  Drawer(uint16_t width, uint16_t height, DrawerSettings *settings);
  ~Drawer() { delete data; }
  virtual void setup() = 0;
  virtual void draw();
  virtual void reset() = 0;
  void setColor(uint16_t x, uint16_t y, uint16_t index);
  void setColor(uint16_t x, uint16_t y, uint8_t r, uint8_t g, uint8_t b);
  void setColorAll(uint8_t r, uint8_t g, uint8_t b) {
    for (int x=0; x<width; x++) {
      for (int y=0; y<height; y++) {
	setColor(x,y,r,g,b);
      }
    }
  }
  uint16_t getPaletteSize() { return settings->palette->getSize(); }
  uint8_t *getData() { return data->getData(); }
  uint16_t getDataLength() { return data->getLength(); }
  DrawerSettings *getSettings() { return settings; }
  void setGamma(float newGamma);

 protected:
  uint16_t width, height;
  Array3D<uint8_t> *data;
  DrawerSettings *settings;
  uint8_t gammaTable[NUM_COLORS];
  float gamma;
  int frameNum;
};

typedef Drawer* DrawerPtr;

#endif
