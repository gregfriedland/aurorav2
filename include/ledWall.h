#ifndef LEDWALL_H
#define LEDWALL_H

#include <stdint.h>
#include <iostream>
#include <string.h>
#include "Drawer.h"
#include <string>
#include "SPI.h"
#include "Palette.h"
#include "Serial.h"
#include "BeatDetect.h"
#include "AudioInput.h"
#include "OSC.h"

#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"

#define USE_AUDIO 0
#define USE_SERIAL 0
#define USE_OSC 1
#define MAX_CMD_ARGS 10

// Structure: 1-byte command, multi-byte payload, end with 255
enum Command {
  CMD_NONE         = 0x00, // not used
  CMD_NEXT_PROGRAM = 0x01, // one arg (up/down)
  CMD_NEXT_PALETTE = 0x02, // one arg (up/down)
  CMD_TEXT_ENTRY   = 0x03, // text string in payload; text entry on ipad
  CMD_SET_SPEED    = 0x04, // one arg
  CMD_SET_COLORCYCLING    = 0x05, // one arg
  CMD_SET_CUSTOM1  = 0x06, // one arg
  CMD_SET_CUSTOM2  = 0x07, // one arg
  CMD_RESET        = 0x08, // one arg (up/down)
  CMD_SET_BRIGHTNESS = 0x09, // one arg
  CMD_SET_MIN_SATURATION = 0x0A, // one arg
  CMD_SET_PROGRAM    = 0x0B, // one arg
  CMD_SET_PALETTE    = 0x0C, // one arg
  CMD_SET_AUDIO_SENSITIVITY1 = 0x0D, // one arg
  CMD_SET_AUDIO_SENSITIVITY2 = 0x0E, // one arg
  CMD_SET_AUDIO_SENSITIVITY3 = 0x0F, // one arg
  CMD_SET_AUDIO_COLOR_CHG1 = 0x10, // one arg
  CMD_SET_AUDIO_COLOR_CHG2 = 0x11, // one arg
  CMD_SET_AUDIO_COLOR_CHG3 = 0x12, // one arg
  CMD_SET_AUDIO_SPEED_CHG1 = 0x13, // one arg
  CMD_SET_AUDIO_SPEED_CHG2 = 0x14, // one arg
  CMD_SET_AUDIO_SPEED_CHG3 = 0x15, // one arg
  CMD_SET_AUDIO_BEAT_LENGTH = 0x16, // one arg

  CMD_SET_TOUCH1 = 0x17, // two args
  CMD_SET_TOUCH2 = 0x18, // two args
  CMD_SET_TOUCH3 = 0x19, // two args
  CMD_SET_TOUCH4 = 0x1A, // two args
  CMD_SET_TOUCH5 = 0x1B, // two args
};


class LEDWall {
 public:
  LEDWall(uint16_t width, uint16_t height, float fps, const char* spiDev, const char *serialDev,
	  uint32_t spiSpeed, const char *paletteFile, uint16_t paletteSize, 
	  int startPalette, uint8_t startVisualization, int audioFrames);
  void run();
  void receiveSerialCmds();
  void processCmd(uint8_t cmd, float *cmdArgs, int numArgs);
  DrawerSettings *getActiveSettings() {
    return visuals[currVisual]->getSettings();
  }

#if USE_AUDIO
  BeatDetect *getBeatDetect() { return bd; }
#endif

  float getSettingByCmd(int cmd);

 private:

  uint16_t width, height;
  float fps;
  SPI *spi;
  vector<PalettePtr> palettes;
  uint16_t currPalette;

#if USE_SERIAL
  Serial *s;
  char serialData[MAX_SERIAL_DATA_SIZE];
  char *serialDataPtr, *serialCmdStart;
#endif

#if USE_OSC
  LEDWallOSC *osc;
#endif

  uint8_t currVisual;
  vector<DrawerPtr> visuals;

#if USE_AUDIO
  BeatDetect *bd;
  AudioInput *audioInput;
#endif
};






#if 0
class OscListener : public osc::OscPacketListener {
 public:
  OscListener(LEDWall *ledWall) { //getActiveSettingsCallback _getActiveSettings,
    //getBeatDetectCallback _getBeatDetect) {
    //osc::OscPacketListener();
    //getActiveSettings = _getActiveSettings;
    //getBeatDetect = _getBeatDetect;
  }
 protected:
  virtual void ProcessMessage( const osc::ReceivedMessage& m, 
			       const IpEndpointName& remoteEndpoint );
};

class LEDWallOSC {
public:
  LEDWallOSC(LEDWall *ledWall) { //getActiveSettingsCallback getActiveSettings, 
    //getBeatDetectCallback getBeatDetect) {
    listener = new OscListener(ledWall);
  }

  void start() {
    socket = new UdpListeningReceiveSocket(
            IpEndpointName( IpEndpointName::ANY_ADDRESS, OSC_PORT ),
            listener );

    pid_t pid = fork();
    if (pid == 0) {                // child
      cout << "Forked OSC process\n";
      socket->RunUntilSigInt();
      cout << "OSC thread done\n";
      exit(0);
    } else if (pid < 0) {            // failed to fork
      cerr << "Failed to fork to listen for OSC packets" << endl;
    } else {                                   // parent
    }
  }

private:
  UdpListeningReceiveSocket *socket;
  OscListener *listener;
  LEDWall *ledWall;
};
#endif

#if 0
typedef DrawerSettings* (*getActiveSettingsCallback)(LEDWall*);
DrawerSettings *getActiveSettings(LEDWall *ledWall) {
  return ledWall->getActiveSettings();
}

typedef BeatDetect* (*getBeatDetectCallback)(LEDWall*);
BeatDetect *getBeatDetect(LEDWall *ledWall) {
  return ledWall->getBeatDetect();
}
#endif

#endif
