#ifndef BEATDETECT_H
#define BEATDETECT_H

#include <stdint.h>
#include <vector>
#include "FFT.h"
#include "util.h"
#include <math.h>
#include "CircularArray.h"

#define HISTORY_SIZE 50
#define SHORT_HISTORY_SIZE 3
#define LONG_HISTORY_SIZE 5
#define MIN_FREQ 100
#define MAX_FREQ 10000
#define MIN_THRESHOLD 0.0f
#define DEFAULT_THRESH_SENSITIVITY 5
#define DEFAULT_BEAT_LENGTH 300    // the default beat length in ms
#define MAX_AUDIO_COLOR_OFFSET 500 // the maximum color index offset from audio beats
#define MAX_BEAT_LENGTH 750 
#define MAX_THRESH_SENSITIVITY 10

class BeatDetect {
 public:
  BeatDetect(int _numBands, int _sampleSize, int _sampleRate);
  ~BeatDetect() {
    delete fft;
    // FIX: free circular arrays
  }
  void update(float *samples);

  float getBeatPos(uint8_t band) {
    // linear
    float diff = lastOnsetTimer[band].elapsedMillis() / float(beatLength[band]);
    if (diff <= 1) return sin(diff*PI);
    else return 0;
  }

  uint8_t getNumBands() { return numBands; }

  void setBeatLength(int band, int length) { beatLength[band] = length; }
  int getBeatLength(int band) { return beatLength[band]; }
  void setAudioSensitivity(int band, float sensitivity) { threshSensitivity[band] = sensitivity;}
  float getAudioSensitivity(int band) { return threshSensitivity[band]; }

 private:
  int numBands, sampleSize, sampleRate;
  FFT *fft;
  vector<int> beatLength;
  vector<float> threshSensitivity, bandFreq;
  vector<bool> analyzeBand;
  vector<CircularArrayWithAvgs*> spectralFlux, spectralFluxSD;
  vector<CircularArray*> onsetHist, threshold, spectrum;
  vector<Timer> lastOnsetTimer;
};

#endif
