#ifndef UTIL_H
#define UTIL_H

#define DEBUG 0

using namespace std;

#include <time.h>
#include <sys/time.h>
#include <stdint.h>
#include <string>

#define PI 3.14159

#if DEBUG
  #define DPRINTF(...) printf(__VA_ARGS__)
#else
  #define DPRINTF(...) 
#endif

#define FALSE 0
#define TRUE 1

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))

inline float radians(float deg) { return deg / 360.0 * 2 * PI; }
inline float degrees(float rad) { return rad * 360.0 / 2 / PI; }

double constrain(double x, double low, double hi);
int constrain(int x, int low, int hi);
double*** mallocDouble3d(uint16_t x, uint16_t y, uint16_t z);
uint8_t*** mallocByte3d(uint16_t x, uint16_t y, uint16_t z);
clock_t millis();
clock_t micros();

double interp(double start, double end, uint32_t index, uint32_t length);
void splitRGB(uint32_t col, uint8_t *r, uint8_t *g, uint8_t *b);
uint32_t combRGB(uint8_t r, uint8_t g, uint8_t b);
void rgbFromHex(string hex, uint8_t *r, uint8_t *g, uint8_t *b);
float dist(float x1, float y1, float x2, float y2);


int getIndex(int ind, int length);
float calcMean(float *data, int dataLength, int startInd, int endInd);
float calcSD(float *data, int dataLength, int startInd, int endInd, float mean);
float calcSD(float *data, int dataLength, int startInd, int endInd);



class Timer {
 public:
  Timer() { gettimeofday(&t, NULL); }
  Timer (const Timer& timer) { t = timer.t; }
  void reset() { gettimeofday(&t, NULL); }
  
  long elapsedMicros() { 
    timeval t2;
    gettimeofday(&t2, NULL);
    return (t2.tv_sec - t.tv_sec) * 1000000 + t2.tv_usec - t.tv_usec;
  }
  long elapsedMillis() { 
    timeval t2;
    gettimeofday(&t2, NULL);
    return (t2.tv_sec - t.tv_sec) * 1000.0 + (t2.tv_usec - t.tv_usec) / 1000.0;
  }

 private:
  timeval t;
};

#endif
