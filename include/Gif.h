#ifndef GIF_H
#define GIF_H

#include <gif_lib.h>

class Gif {
 public:
  Gif(const char *filename) {
    //int error;
    //if((gif = DGifOpenFileName(filename, &error)) == NULL) {
    //  printf("Error opening gif '%s': %d\n", filename, error);
    if((gif = DGifOpenFileName(filename)) == NULL) {
      printf("Error opening gif '%s'\n", filename);
      PrintGifError();
      return;
    }

    if (DGifSlurp(gif) == GIF_ERROR) {
      printf("Error slurping gif '%s'\n", filename);
      PrintGifError();
      return;
    }

    if (gif->SColorMap->BitsPerPixel != 8) {
      printf("Invalid pixel depth: %d\n", gif->SColorMap->BitsPerPixel);
      return;
    }

    printf("Opened GIF '%s' with width %d, height %d, and %d frames\n",
	   filename, gif->SWidth, gif->SHeight, gif->ImageCount);
  }

  ~Gif() {
    DGifCloseFile(gif);
  }

  void loadIntoArray(Array3D<uint8_t> *data, int frame) {
    if (frame >= gif->ImageCount) {
      printf("Attempted to access a frame beyond the length of the GIF\n");
      return;
    }

    uint32_t width = gif->SWidth;
    uint32_t height = gif->SHeight;

    if (data->getDimX() != 3 || data->getDimY() != width ||
	data->getDimZ() != height) {
      printf("Array dimensions don't match GIF size\n");
      return;
    }
    
    SavedImage *img = &(gif->SavedImages[frame]);
    for (uint32_t y=0; y<height; y++) {
      for (uint32_t x=0; x<width; x++) {
	uint8_t gifColor = img->RasterBits[y*width + x];
	uint8_t r=0,g=0,b=0;
	getColorRGB(frame, gifColor, &r, &g, &b);
	
	(*data)(0,x,height-y-1) = r;
	(*data)(1,x,height-y-1) = g;
	(*data)(2,x,height-y-1) = b;
      }
    }
  }

  int getNumFrames() {
    return gif->ImageCount;
  }

 private:
  void getColorRGB(int frame, uint8_t gifColor, uint8_t *r, uint8_t *g, uint8_t *b) {
    if (gifColor >= gif->SColorMap->ColorCount) {
      printf("Attempted to access GIF color outside of the map\n");
      return;
    }

    //GifColorType *gifRGB = &(gif->SavedImages[frame].ImageDesc.ColorMap->Colors[gifColor]);
    GifColorType *gifRGB = &(gif->SColorMap->Colors[gifColor]);
    *r = gifRGB->Red;
    *g = gifRGB->Green;
    *b = gifRGB->Blue;
  }

  GifFileType *gif;
};

#endif
