#ifndef TEXTDRAWER_H
#define TEXTDRAWER_H

#include "Drawer.h"

class TextDrawer : public Drawer {
 public:
 TextDrawer(int width, int height, DrawerSettings *settings) :
  Drawer(width, height, settings) {
  }

  void setup() { t = 0; }
  void reset() { t = 0; }
  void draw();

  void drawChar(int x, int y, char c, uint8_t r, uint8_t g, uint8_t b, int size);
  void drawChar(int x, int y, char c, int index, int size);
  
 private:
  float t;
  int index;
};

#endif
