#ifndef ARRAY3D_H
#define ARRAY3D_H

#include <stdio.h>
#include <stdlib.h>

 // adapted from http://www.cplusplus.com/forum/articles/17108/

template <typename T>
class Array3D
{
public:
  // constructor
  Array3D(unsigned _nx,unsigned _ny,unsigned _nz)
    : nx(_nx), ny(_ny), nz(_nz), pA(0)
  {
    if(_nx > 0 && _ny > 0 && _nz > 0)
      pA = new T[nx * ny * nz];
    else {
      printf("Invalid array bounds\n");
      exit(1);
    }
  }

  // destructor
  ~Array3D()
  {
    delete[] pA;
  }

  // indexing (parenthesis operator)
  const T& operator () (unsigned x,unsigned y,unsigned z) const
  {  return pA[ z*nx*ny + y*nx + x ];   }

  T& operator () (unsigned x,unsigned y,unsigned z)
  {  return pA[ z*nx*ny + y*nx + x ];   }

  // get dims
  unsigned getDimX() const { return nx; }
  unsigned getDimY() const { return ny; }
  unsigned getDimZ() const { return nz; }

  T* getData() const { return pA; }
  uint32_t getLength() { return nx*ny*nz; }

private:
  unsigned nx, ny, nz;
  T*       pA;

  // to prevent unwanted copying:
  Array3D(const Array3D<T>&);
  Array3D& operator = (const Array3D<T>&);
};

#endif
