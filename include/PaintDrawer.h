#ifndef PAINTDRAWER_H
#define PAINTDRAWER_H

#include "Drawer.h"
#include "Array2D.h"

class PaintDrawer : public Drawer {
 public:
 PaintDrawer(int width, int height, DrawerSettings *settings) :
  Drawer(width, height, settings) {
    canvas = new Array2D<int>(width, height);
    clear();
  }

  void setup() {}
  void reset() { clear(); }
  void draw() {
    Drawer::draw();

    for (int i=0; i<MAX_TOUCHES; i++) {
      int x, y;
      if (settings->isTouching(i, &x, &y)) {
	// set the index in the canvas array but don't allow zero if we've touched
	int palSize = settings->palette->getSize();
	(*canvas)(x,y) = (frameNum + i * palSize / MAX_TOUCHES) % (palSize - 1) + 1;
      }
    }

    for (int x=0; x<width; x++) {
      for (int y=0; y<height; y++) {
	if ((*canvas)(x,y) == 0) {
	  setColor(x,y,0,0,0);
	} else {
	  setColor(x,y,(*canvas)(x,y));
	}
      }
    }
  }

  void clear() {
    for (int x=0; x<width; x++) {
      for (int y=0; y<height; y++) {
        (*canvas)(x,y) = 0;
      }
    }
  }
  
 private:
  Array2D<int> *canvas;
};

#endif
