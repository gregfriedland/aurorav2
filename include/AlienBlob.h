#ifndef ALIENBLOB_H
#define ALIENBLOB_H

#include "Drawer.h"
#include "util.h"
#include "Perlin.h"
#include <math.h>

#define MAX_HARMONICS 3

class AlienBlob : public Drawer {
 public:
 AlienBlob(uint16_t width, uint16_t height, DrawerSettings *settings) :
  Drawer(width, height, settings) {
    // precalculate 1 period of the sine wave (360 degrees)
    for (int i=0; i<360; i ++) sineTable[i] = sin(radians(i));
  }

  void setup() {
    dThresh = 90;
    incr = 0.03125;
    xoffIncr = 0.003; //0.3;
    yoffIncr = 0.0007; //0.07;
    zoffIncr = 0.1;
    noiseMult = 5; //3   
    xoff = yoff = zoff = 0;
  }

  void reset() { zoff = 0; }

  void draw() {
    Drawer::draw();

    float d, h, n;
    float xx;
    float yy = 0; 
    int w2 = width / 2;
    int h2 = height / 2;
 
    int nd = ceil(MAX_HARMONICS * settings->custom1);
    Perlin perlin(nd, 1.0, 0.9, 100); // num harmonics, unused, harmonic falloff, seed

    float multiplier = settings->custom2 * 10;
    for (int y=0; y<height; y++) {
      xx = 0; 
      for (int x=0; x<width; x++) {
        d = dist(x, y, w2, h2) * 0.025;
        //if (d * 20 <= dThresh) {
	n = fabs(perlin.Get(xx*multiplier, yy*multiplier, zoff));
	//printf("%d %d %f ", x, y, n);
    
          // use pre-calculated sine results
          h = sineTable[int(degrees(d + n * noiseMult)) % 360] / 2 + 0.5;
	             
          // determine pixel color
          setColor(x,y,int(h*(settings->palette->getSize()-1)));
	  //} else {
          //setColor(x,y,0);
	  //}
         
        xx += incr;
      }
       
      yy += incr;
    }
     
    // move through noise space -> animation
    zoff += zoffIncr * settings->speed;
  }

 private:
  float xoff, yoff, zoff;
  float dThresh, incr, xoffIncr, yoffIncr, zoffIncr, noiseMult;
  float sineTable[360];
};

#endif
