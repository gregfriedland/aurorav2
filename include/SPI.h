#ifndef SPICOMM_H
#define SPICOMM_H

#include <stdint.h>

class SPI {
 public:
  SPI(const char *dev, uint32_t speed);
  void transfer(uint8_t *data, uint16_t nbytes);
 private:
  int fd;
  uint32_t speed;
};

#endif
