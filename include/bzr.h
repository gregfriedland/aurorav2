#ifndef BZR_H
#define BZR_H

#include <stdint.h>
#include "Drawer.h"
#include "Array3D.h"

class Bzr : public Drawer {
 public:
  Bzr(uint16_t width, uint16_t height, DrawerSettings *settings);
  void randomDots(uint16_t minx, uint16_t miny, uint16_t maxx, uint16_t maxy);
  void setAll(uint16_t minx, uint16_t miny, uint16_t maxx, uint16_t maxy,
	      float aval, float bval, float cval);
  void setup();
  void draw();
  void reset();

 private:
  uint16_t aWidth, aHeight;
  uint8_t p, q;
  Array3D<float> *a, *b, *c;
  uint16_t state;
  int perturbCounter;
};

#endif
