#ifndef OSC_H
#define OSC_H

#include <iostream>
#include <string.h>
#include <pthread.h>
#include <map>

#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"
#include "util.h"

#define OSC_IN_PORT 7003
#define OSC_OUT_PORT 9000

class LEDWall;

class OscListener : public osc::OscPacketListener {
 public:
  OscListener(LEDWall *_ledWall, std::map<std::string, int> *_cmdMap) { 
    ledWall = _ledWall; 
    cmdMap = _cmdMap;
  }
  IpEndpointName *getRemoteEndpoint() { return &remoteEndpoint; }

 protected:
  virtual void ProcessMessage( const osc::ReceivedMessage& m, 
			       const IpEndpointName& _remoteEndpoint );
  LEDWall *ledWall;
  std::map<std::string, int> *cmdMap;
  IpEndpointName remoteEndpoint;
};



class LEDWallOSC {
public:
  LEDWallOSC(LEDWall *_ledWall) {
    ledWall = _ledWall;
    registerCommands();
    listener = new OscListener(_ledWall, &cmdMap);
    transmitSocket = NULL;
  }
  void start();
  void send(const char *addr, const char *msg);
  void sendSettings();
  void registerCommands();

private:
  UdpListeningReceiveSocket *socket;
  OscListener *listener;
  Timer lastSendTimer;
  UdpTransmitSocket *transmitSocket;
  std::map<std::string, int> cmdMap;
  LEDWall *ledWall;
};


#endif
