#ifndef ARRAY2D_H
#define ARRAY2D_H

#include <stdio.h>
#include <stdlib.h>

 // adapted from http://www.cplusplus.com/forum/articles/17108/

template <typename T>
class Array2D
{
public:
  // constructor
  Array2D(unsigned _nx,unsigned _ny)
    : nx(_nx), ny(_ny), pA(0)
  {
    if(_nx > 0 && _ny > 0)
      pA = new T[nx * ny];
    else {
      printf("Invalid array bounds\n");
      exit(1);
    }
  }

  // destructor
  ~Array2D()
  {
    delete[] pA;
  }

  // indexing (parenthesis operator)
  const T& operator () (unsigned x,unsigned y) const
  {  return pA[ y*nx + x ];   }

  T& operator () (unsigned x,unsigned y)
  {  return pA[ y*nx + x ];   }

  // get dims
  unsigned getDimX() const { return nx; }
  unsigned getDimY() const { return ny; }

  T* getData() const { return pA; }
  uint32_t getLength() { return nx*ny; }

private:
  unsigned nx, ny;
  T*       pA;

  // to prevent unwanted copying:
  Array2D(const Array2D<T>&);
  Array2D& operator = (const Array2D<T>&);
};

#endif
