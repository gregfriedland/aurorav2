// Utility class holding data in a circular buffer and implementing basic exponential moving averages

#ifndef CIRCULARARRAY_H
#define CIRCULARARRAY_H

#include <math.h>
#include "util.h"
#include <math.h>
#include <stdlib.h>

class CircularArray {
 public:
  CircularArray(int _length) {
    length = _length;
    data = (float*) malloc(sizeof(float) * length);
    index = prevIndex = avgLength = sdLength = -1;
  }

  ~CircularArray() { 
    free(data);
  }
  
  int getIndex() { return index; }
  int getPrevIndex() { return prevIndex; }
  
  void add(float val) {
    prevIndex = index;
    index++;
    if (index == length) index = 0;
    data[index] = val;    
  }

  float getPrev() { 
    if (prevIndex == -1) return 0; 
    return data[prevIndex]; 
  }
  float get() { 
    if (index == -1) return 0; 
    return data[index]; 
  }
  float get(int ind) { return data[getIndex(ind)]; }

  int getLength() { return length; }
  int getIndex(int ind) { return (ind + length) % length; }
    
  float maxVal() {
    float m = 0;
    for (int i=0; i<length; i++) m = max(m, data[i]);
    return m;
  }
  
  float* getArray() { return data; }
  float mean() { return mean(0, length-1); }
  float mean(int startInd, int endInd) { return calcMean(data, length, startInd, endInd); }
  float sd(float mean) { return sd(0, length-1, mean); }
  float sd(int startInd, int endInd, double mean) { return calcSD(data, length, startInd, endInd, mean); }
  float sd(int startInd, int endInd) { return calcSD(data, length, startInd, endInd); }

 protected:
  int index, prevIndex, length, avgLength, sdLength;
  float *data;
  float *movingAvg, *movingSD;
};


class CircularArrayWithAvgs : public CircularArray {
 public:
  CircularArrayWithAvgs(int length, int _avgLength, int _avgLength2) 
   : CircularArray(length) {
    avgLength = _avgLength;
    avgLength2 = _avgLength2;
    alpha = 2.0 / (avgLength + 1);
    alpha2 = 2.0 / (avgLength2 + 1);
    ema = (float *) malloc(length*sizeof(float));
    ema2 = (float *) malloc(length*sizeof(float));
  }

  ~CircularArrayWithAvgs() {
    free(ema);
    free(ema2);
  }

  void add(float val) {
    CircularArray::add(val);
    ema[index] = data[index] * alpha + (ema[getIndex(index-1)] * (1-alpha));
    ema2[index] = data[index] * alpha2 + (ema2[getIndex(index-1)] * (1-alpha2));
  }
  
  float getEMA1(int ind) {
    return ema[getIndex(ind)];
  }

  float getEMA2(int ind) {
    return ema2[getIndex(ind)];
  }
 private:
  int avgLength, avgLength2;
  float alpha, alpha2;
  float *ema, *ema2;


};   

#endif
